This is my automation framework created in order to run several tests on the youtube.com website. It was done using python and pytest with POM (Page object model) design pattern. 

## Test cases implemented: 

##### 1. test_comment_your_video.py

1. Log in
2. Open the logged in user's channel
3. Open an uploaded video
4. Write a comment
5. Validation of the written comment

##### 2. test_open_6th.py

1. Log in
2. Open a random video
3. Click on the 6th element in the recommendation list on the right side

##### 2. test_search_and_like.py

1. Log in
2. Search a video on the main youtube page with a certain text
3. Open that video
4. Hit the "like" button
